package identifier;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


public class IdentifierTest {
	/*
	 * TestSetFUNC: A,B,C,D,E
	 * TestSetNOS: B,C,D,F,G
	 * TestSetARESTAS: B,C,D,F,G
	 * TestSetLI: B,C,D,(N.E),H,F
	 * TestSetESGTD: B,C,D,F,G
	 * 
	 */
	
	
	@Test
	void teste_A() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("abc5");
		assertEquals("Valido", result);
	}
	
	@Test
	void teste_B() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("");
		assertEquals("Invalido: Identificador Nulo", result);
	}
	
	@Test
	void teste_C() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("abcdefg");
		assertEquals("Invalido: Identificador deve ter no maximo 6 caracteres", result);
	}
	
	@Test
	void teste_C_2() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("abcdef");
		assertEquals("Invalido: Identificador deve ter no maximo 6 caracteres", result);
	}
	
	@Test
	void teste_D() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("123");
		assertEquals("Invalido: Identificador deve iniciar com letra", result);
	}
	
	@Test
	void teste_E() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("aa*bb");
		assertEquals("Invalido: Identificador deve conter somente letras e digitos", result);
	}
	
	@Test
	void teste_F() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("a*");
		assertEquals("Invalido: Identificador deve conter somente letras e digitos", result);
	}
	
	@Test
	void teste_G() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("ab");
		assertEquals("Valido", result);
	}
	
	@Test
	void teste_H() {
		Identifier id = new Identifier();
		String result = id.validateIdentifier("a");
		assertEquals("Valido", result);
	}
	
	// Testes de coverage
	@Test
	void test_valid_s_true_1() {
		Identifier id = new Identifier();
		Boolean result = id.valid_s('A');
		assertEquals(true, result);
	}
	
	@Test
	void test_valid_s_true_2() {
		Identifier id = new Identifier();
		Boolean result = id.valid_s('a');
		assertEquals(true, result);
	}
	
	@Test
	void test_valid_s_true_3() {
		Identifier id = new Identifier();
		Boolean result = id.valid_s('Z');
		assertEquals(true, result);
	}
	
	@Test
	void test_valid_s_true_4() {
		Identifier id = new Identifier();
		Boolean result = id.valid_s('z');
		assertEquals(true, result);
	}
	
	@Test
	void test_valid_s_false() {
		Identifier id = new Identifier();
		Boolean result = id.valid_s('5');
		assertEquals(false, result);
	}
	
	@Test
	void test_valid_f_true() {
		Identifier id = new Identifier();
		Boolean result = id.valid_f('0');
		assertEquals(true, result);
	}
	
	@Test
	void test_valid_f_true2() {
		Identifier id = new Identifier();
		Boolean result = id.valid_f('9');
		assertEquals(true, result);
	}
	
	// Testes de integração
	
	@Test
	void intraMetodoTest1() {
		Identifier id = new Identifier();
		Boolean result = id.valid_f('9');
		assertEquals(true, result);
	}
}
